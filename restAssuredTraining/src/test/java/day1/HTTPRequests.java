package day1;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.HashMap;

import org.testng.annotations.Test;

/*
given()
    content type, set cookies, add auth, add param, set headers info etc

when()
	get, post, put, delete

then()
	validate status code, extract response, extract headers cookies and response body etc
*/


public class HTTPRequests {
	
	int id;
	
	@Test(priority = 1)
	void getUser() {
		given()
		
		.when()
			.get("https://reqres.in/api/users?page=2")
		
		.then()
			.statusCode(200)
			.body("page", equalTo(2))
			.log().all();
		
	}
	
	@Test(priority = 2)
	void createUser() {
		HashMap hm = new HashMap();
		hm.put("name", "Lalit");
		hm.put("job", "IT Services");
		
		id = given()
			.contentType("application/json")
			.body(hm)
		.when()
			.post("https://reqres.in/api/users")
			.jsonPath().getInt("id");
		
//		.then()
//			.statusCode(201)
//			.log().all();
	}
	
	@Test(priority=3, dependsOnMethods = {"createUser"})
	void updateUser() {
		HashMap hm = new HashMap();
		hm.put("name", "Lalit Dixit");
		hm.put("job", "Learner");
		
		given()
			.contentType("application/json")
			.body(hm)
		.when()
			.put("https://reqres.in/api/users/"+id)
		.then()
			.statusCode(200)
			.log().all();
	}
	
	@Test(priority=4, dependsOnMethods= {"createUser", "updateUser"})
	void deleteUser() {
		given()
		.when()
			.delete("https://reqres.in/api/users/"+id)
		.then()
			.statusCode(204)
			.log().all();
	}
	
	

}
