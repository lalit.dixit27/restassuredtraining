package day2;

/*
 Different ways to create post request body
 1) Using Hashmap
 2) using org.json library
 3) using POJO
 4) using external JSON file
 */
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.annotations.Test;

public class DifferentWaysToCreatePostRequestBody {

	// POST request body using HashMap

//	@Test(priority=1)
	void testPostUsingHashMap() {
		HashMap data = new HashMap();
		data.put("name", "Lalit");
		data.put("location", "India");
		data.put("phone", "123456");

		String[] courseArr = { "C", "C++" };

		data.put("courses", courseArr);
		
		given()
			.contentType("application/json")
			.body(data)
		.when()
			.post("http://localhost:3000/students")
		.then()
			.statusCode(201)
			.body("name", equalTo("Lalit"))
			.body("location", equalTo("India"))
			.body("phone", equalTo("123456"))
			.body("courses[0]", equalTo("C"))
			.body("courses[1]", equalTo("C++"))
			.header("Content-Type", "application/json; charset=utf-8")
			.log().all();
	}	
	
	// POST request body using org.json library

//	@Test(priority=1)
	void testPostUsingJsonLibrary() {
		
		JSONObject data = new JSONObject();
		data.put("name", "Lalit");
		data.put("location", "India");
		data.put("phone", "123456");

		String[] courseArr = { "C", "C++" };

		data.put("courses", courseArr);
		
		given()
			.contentType("application/json")
			.body(data.toString())
		.when()
			.post("http://localhost:3000/students")
		.then()
			.statusCode(201)
			.body("name", equalTo("Lalit"))
			.body("location", equalTo("India"))
			.body("phone", equalTo("123456"))
			.body("courses[0]", equalTo("C"))
			.body("courses[1]", equalTo("C++"))
			.header("Content-Type", "application/json; charset=utf-8")
			.log().all();
	}	
	
	// POST request body using POJO Class 

//	@Test(priority=1)
	void testPostUsingPOJO() {
		
		POJO_PostRequest data = new POJO_PostRequest();
		data.setName("Lalit");
		data.setLocation("India");
		data.setPhone("123456");
		String[] courseArr = { "C", "C++" };
		data.setCourses(courseArr);
		
		given()
			.contentType("application/json")
			.body(data)
		.when()
			.post("http://localhost:3000/students")
		.then()
			.statusCode(201)
			.body("name", equalTo("Lalit"))
			.body("location", equalTo("India"))
			.body("phone", equalTo("123456"))
			.body("courses[0]", equalTo("C"))
			.body("courses[1]", equalTo("C++"))
			.header("Content-Type", "application/json; charset=utf-8")
			.log().all();
	}	
	
	// POST request body using external Json file 

	@Test(priority=1)
	void testPostUsingExternalJsonFile() throws FileNotFoundException {
		
		File f = new File(".//body.json");
		FileReader fr = new FileReader(f);
		JSONTokener jt = new JSONTokener(fr);
		
		JSONObject data = new JSONObject(jt);
		
		
		given()
			.contentType("application/json")
			.body(data.toString())
		.when()
			.post("http://localhost:3000/students")
		.then()
			.statusCode(201)
			.body("name", equalTo("Lalit"))
			.body("location", equalTo("India"))
			.body("phone", equalTo("123456"))
			.body("courses[0]", equalTo("C"))
			.body("courses[1]", equalTo("C++"))
			.header("Content-Type", "application/json; charset=utf-8")
			.log().all();
	}	
	
	@Test(priority=2)
	void testDelete() {
		given()
		.when()
			.delete("http://localhost:3000/students/4")
		.then()
			.statusCode(200);
	}

}
