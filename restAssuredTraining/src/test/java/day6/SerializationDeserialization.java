package day6;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;




//POJO   -----Serialize----> JSON Object  -----Deserialize----> POJO

public class SerializationDeserialization {

	
	//POJO   ---------> JSON (Serialization)
	@Test(priority=1)
	void convertPojoToJson() throws JsonProcessingException {
		
		Student stuPojo = new Student();
		stuPojo.setName("Scott");
		stuPojo.setPhone("123456");
		stuPojo.setLocation("France");
		
		String [] coursesArr = {"C", "C++"};
		stuPojo.setCourses(coursesArr);
		
		//convert Java object to Json object
		
		ObjectMapper objMapper = new ObjectMapper();
		String jsonData = objMapper.writerWithDefaultPrettyPrinter().writeValueAsString(stuPojo);
		System.out.println(jsonData);
	}
	
	//JSON   ---------> POJO (Deserialization)
		@Test(priority=2)
		void convertJsonToPojo() throws JsonProcessingException {
		
			String jsonData = "{\"name\" : \"Scott\",\"location\" : \"France\",\"phone\" : \"123456\",\"courses\" : [ \"C\",\"C++\" ]}";
		
			ObjectMapper objMapper = new ObjectMapper();
			Student studentPojo = objMapper.readValue(jsonData, Student.class);
			System.out.println(studentPojo.getName());
			System.out.println(studentPojo.getLocation());
			System.out.println(studentPojo.getPhone());
			System.out.println(studentPojo.getCourses()[0]);
			System.out.println(studentPojo.getCourses()[1]);
			
		}
	
}
