package day3;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.response.Response;

public class CookiesDemo {

	@Test(priority=1)
	void testCookies() {
		
		given()
		
		.when()
			.get("https://www.google.com/")
		.then()
			.cookie("AEC","ARSKqsIvh0zSiwZUFKfDPZnOqW5EQRpgXvnno965CA3s5NzsY6YwsFRd6Q")
			.log().all();
	}
	
	@Test(priority=2)
	void getCookiesInfo() {
		
		Response res = given()
					   .when()
					    	.get("https://www.google.com/");
		
		String cookie = res.getCookie("AEC");
//		System.out.println("Value of cookie is ==> "+cookie);
		Map<String, String> cookieMap = res.getCookies();
//		System.out.println(cookieMap.keySet());
		
		for(String key: cookieMap.keySet()) {
			System.out.println(key+" ===> "+cookieMap.get(key));
		}
		
		
	}
}
