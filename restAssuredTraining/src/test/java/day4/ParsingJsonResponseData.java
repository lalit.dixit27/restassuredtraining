package day4;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.List;

public class ParsingJsonResponseData {

	@Test(priority=1)
	void testJsonResponse() {
		
	//Approach 1
		
	given()
		.contentType(ContentType.JSON)
	.when()
		.get("http://localhost:3000/store")
	.then()
		.statusCode(200)
		.header("Content-Type","application/json; charset=utf-8")
		.body("book[3].author", equalTo("J.R.R. Tolkien"));
		
	//Approach 2
		
	Response res = given()
						.contentType(ContentType.JSON)
					.when()
						.get("http://localhost:3000/store");
	
	Assert.assertEquals(res.getStatusCode(), 200); //Validation 1
	Assert.assertEquals(res.getContentType(), "application/json; charset=utf-8"); //Validation 2
	
	String bookName = res.jsonPath().get("book[3].title").toString();
	Assert.assertEquals(bookName, "The Lord of the Ring");
	
	List<String> bookAuthors = res.jsonPath().getList("book.author");
	System.out.println(bookAuthors);
	Assert.assertTrue(bookAuthors.contains("Evelyn Waugh"));
	
	
	for(String author : bookAuthors) {
		System.out.println(author);
	}

	
	}
	
	@Test(priority=2)
	void testJsonResponseBodyData() {
		
		Response res = given()
							.contentType(ContentType.JSON)
						.when()
							.get("http://localhost:3000/store");
		
		//JSONObject class
		
		JSONObject jo = new JSONObject(res.asString());
		
//		for(int i=0; i< jo.getJSONArray("book").length(); i++) {
//			String bookTitle = jo.getJSONArray("book").getJSONObject(i).get("title").toString();
//			System.out.println(bookTitle);
//		}
		
		//Search for title of book in the response
		boolean status  = false;
		
		for(int i=0; i< jo.getJSONArray("book").length(); i++) {
			String bookTitle = jo.getJSONArray("book").getJSONObject(i).get("title").toString();
			if(bookTitle.equals("Moby Dick")) {
				status = true;
				break;
			}
		}
		
		//Validate to the total of prices with expected price
		
		double actualPriceTotal = 0.0;
		for(int i=0; i< jo.getJSONArray("book").length(); i++) {
			actualPriceTotal += jo.getJSONArray("book").getJSONObject(i).getDouble("price");
			
			
		}
		System.out.println(actualPriceTotal);
		Assert.assertEquals(actualPriceTotal, 526.00);
	
	}
}
