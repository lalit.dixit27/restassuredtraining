package day7;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;



public class Authentications {

//	@Test(priority=1)
	void testBasicAuthentications() {
		given()
			.auth().basic("postman", "password")
		.when()
		    .get("https://postman-echo.com/basic-auth")
		.then()
			.statusCode(200)
			.body("authenticated", equalTo(true))
			.log().all();
	}
	
//	@Test(priority=2)
	void testDigestAuthentications() {
		given()
			.auth().digest("postman", "password")
		.when()
		    .get("https://postman-echo.com/basic-auth")
		.then()
			.statusCode(200)
			.body("authenticated", equalTo(true))
			.log().body();
	}
	
//	@Test(priority=3)
	void testPreemptiveAuthentications() {
		given()
			.auth().preemptive().basic("postman", "password")
		.when()
		    .get("https://postman-echo.com/basic-auth")
		.then()
			.statusCode(200)
			.body("authenticated", equalTo(true))
			.log().body();
	}
	
//	@Test(priority=4)
	void testBearerTokenAuthentications() {
		String bearerToken = "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";
		
		given()
			.headers("Authorization", "Bearer "+bearerToken)
		.when()
			.get("https://api.github.com/user/repos")
		.then()
			.statusCode(200)
			.log().all();
		
	}
	
//	@Test(priority=5)
	void testOAuth1Authentications() {
		
		given()
			.auth().oauth("consumerKey", "consumerSecrete", "accessToken", "tokenSecrete")  //this is for oauth1 authentication
		.when()
			.get("url")
		.then()
			.statusCode(200)
			.log().all();
	}
	
//	@Test(priority=6)
	void testOAuth2Authentications() {
		given()
		.auth().oauth2("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")
	.when()
		.get("https://api.github.com/user/repos")
	.then()
		.statusCode(200)
		.log().all();
	}
	
	@Test(priority=7)
	void testAPIKeyAuthentications() {
		
		given()
			.queryParam("appid", "18eb1c43355c36efc6eff40f44508c68")  //appid is API key
		.when()
			.get("https://api.openweathermap.org/data/2.5/forecast/daily?q=London&units=metric&cnt=7")
		.then()
			.statusCode(200)
			.log().all();
	}
}
