package day8;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import org.testng.ITestContext;
import org.testng.annotations.Test;

public class GetUser {

	@Test
	void test_getUser(ITestContext context) {
		
//		int id = (Integer) context.getAttribute("user_id");   // this should come from create_user request
		int id = (Integer) context.getSuite().getAttribute("user_id");  
		
		String bearerToken = "16472695a2f454806341971b1ea5fa62f29365459ba108a444aca76a2b1f85c3";
		
		given()
			.headers("Authorization", "Bearer "+bearerToken)
			.pathParam("id", id)
		.when()
			.get("https://gorest.co.in/public/v2/users/{id}")
		.then()
			.statusCode(200)
			.log().all();
	}
}
