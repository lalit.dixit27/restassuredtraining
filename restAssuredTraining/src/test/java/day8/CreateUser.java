package day8;

import org.testng.annotations.Test;
import org.json.JSONObject;
import org.testng.ITestContext;
import org.testng.annotations.Test;
import com.github.javafaker.Faker;

import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class CreateUser {

	@Test
	void test_createUser(ITestContext context) {
		
		Faker faker = new Faker();
		
		JSONObject data = new JSONObject();
		data.put("name", faker.name().fullName());
		data.put("gender", "Male");
		data.put("email", faker.internet().emailAddress());
		data.put("status", "Inactive");
		
		String bearerToken = "16472695a2f454806341971b1ea5fa62f29365459ba108a444aca76a2b1f85c3";
		
		int id = given()
					.header("Authorization", "Bearer "+bearerToken)
					.contentType("application/json")
					.body(data.toString())
				.when()
					.post("https://gorest.co.in/public/v2/users")
					.jsonPath().getInt("id");
		
		System.out.println("Generated id is "+id);
//		context.setAttribute("user_id", id);
		context.getSuite().setAttribute("user_id", id);
		
	}
	
}
