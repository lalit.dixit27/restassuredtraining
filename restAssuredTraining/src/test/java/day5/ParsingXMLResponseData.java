package day5;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

public class ParsingXMLResponseData {

//	@Test(priority=1)
	void testXmlResponse() {
		
		//Approach 1
		
		given()
		.when()
			.get("http://restapi.adequateshop.com/api/Traveler?page=1")
		.then()
			.statusCode(200)
			.header("Content-Type","application/xml; charset=utf-8")
			.body("TravelerinformationResponse.page", equalTo("1"))
			.body("TravelerinformationResponse.travelers.Travelerinformation[0].name", equalTo("Developer"));
		
		
		//Approach 2
		
		Response res = given()
						.when()
							.get("http://restapi.adequateshop.com/api/Traveler?page=1");
			
		Assert.assertEquals(res.getStatusCode(), 200);
		Assert.assertEquals(res.getHeader("Content-Type"), "application/xml; charset=utf-8");
		Assert.assertEquals(res.xmlPath().get("TravelerinformationResponse.page"), "1");
		
		Assert.assertEquals(res.xmlPath().get("TravelerinformationResponse.travelers.Travelerinformation[0].name"), "Developer");
		
		String sample = res.xmlPath().getString("TravelerinformationResponse.travelers.Travelerinformation[0].name");
		System.out.println(sample);
		
	}
	
	@Test(priority=2)
	void testXmlResponseBody() {
		
		Response res = given()
						.when()
							.get("http://restapi.adequateshop.com/api/Traveler?page=1");
			
		//XmlPath Class
		XmlPath xmlObj = new XmlPath(res.asString());
		
		List<String> travelers = xmlObj.getList("TravelerinformationResponse.travelers.Travelerinformation");
		System.out.println(travelers.size());
		Assert.assertEquals(travelers.size(), 10);
		
		//verify a particular traveler name is present in response
		
		boolean status = false;
		List<String> travelersName = xmlObj.getList("TravelerinformationResponse.travelers.Travelerinformation.name");
		System.out.println(travelersName);
		for(String name: travelersName) {
			if(name.equals("asdasd")) {
				status= true;
				break;
			}
		}
		Assert.assertTrue(status);
	}
	
}
