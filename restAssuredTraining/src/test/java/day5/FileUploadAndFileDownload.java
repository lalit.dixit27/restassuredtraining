package day5;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.io.File;

import org.testng.annotations.Test;

public class FileUploadAndFileDownload {

//	@Test(priority=1)
	void singleFileUpload() {
		
		File myFile = new File("Path_of_file");
		
		given()
			.multiPart("file", myFile)
			.contentType("multipart/form-data")
		.when()
			.post("http://localhost:8080/uploadFile")
		.then()
			.statusCode(200)
			.body("fileName", equalTo("Test1.txt"))
			.log().all();
	}
	
	@Test(priority=2)
	void multipleFilesUpload() {
		
		File myFile1 = new File("Path_of_file1");
		File myFile2 = new File("Path_of_file2");
		
		given()
			.multiPart("files", myFile1)
			.multiPart("files", myFile2)
			.contentType("multipart/form-data")
		.when()
			.post("http://localhost:8080/uploadFile")
		.then()
			.statusCode(200)
			.body("fileName", equalTo("Test1.txt"))
			.log().all();
	}
	
	@Test(priority=3)
	void multipleFilesUpload2() {
		
		File myFile1 = new File("Path_of_file1");
		File myFile2 = new File("Path_of_file2");
		
		File[] myFileArr = { myFile1, myFile2};
		
		given()
			.multiPart("files", myFileArr)   // may not work for all kinds of API
			.contentType("multipart/form-data")
		.when()
			.post("http://localhost:8080/uploadFile")
		.then()
			.statusCode(200)
			.body("fileName", equalTo("Test1.txt"))
			.log().all();
	}
	
	void fileDownload() {
		
		given()
		.when()
			.get("http://localhost:8080/downloadFile/Test1.txt")
		.then()
			.statusCode(200)
			.log().body();
	}
}
